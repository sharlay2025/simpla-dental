/* Beer slider */
document.addEventListener('DOMContentLoaded', function() {
    let $baSliders = document.querySelectorAll('[data-element="ba-slider"]:not([data-element-v2="ba-slider"])');
    const $baSlidersV2 = document.querySelectorAll('[data-element-v2="ba-slider"]');
    let i = 0;

    if ($baSliders.length + $baSlidersV2.length === 0) {
        return false;
    }

    for (i = 0; i < $baSliders.length; i++) {
        let $baSlider = new BeerSlider($baSliders[i], { start: 25 });
    }

    $baSlidersV2.forEach((slider) => {
      const baSlider = new BeerSlider(slider, { start: 50 });
    });
});

/* Collapsible */
document.addEventListener('DOMContentLoaded', function() {
    let $collapsibleToggles = document.querySelectorAll('[data-element="collapsible-toggle"]');
    let $collapsibleContents = document.querySelectorAll('[data-element="collapsible-content"]');
    let i = 0;

    if ($collapsibleToggles.length === 0) {
        return false;
    }

    responsiveCollapsibleHeight();

    for (i = 0; i < $collapsibleToggles.length; i++) {
        $collapsibleToggles[i].addEventListener("click", function(e) {
            let $toggle = this;
            let toggleTextOn = $toggle.dataset.textOn;
            let toggleTextOff = $toggle.dataset.textOff;
            let toggleDisabledOn = $toggle.dataset.disabledOn;
            let windowWidth = window.innerWidth;

            if ((toggleDisabledOn === 'tablet' && windowWidth >= bpMD) || (toggleDisabledOn ==- 'desktop' && windowWidth >= bpXL)) {
                if ($toggle.hasAttribute('href')) {
                    window.location = this.href;
                } else {
                    e.preventDefault();
                }
            } else {
                e.preventDefault();

                let $collapsible   = $toggle.closest('[data-component="collapsible"]');
                let $content = $collapsible.querySelector('[data-element="collapsible-content"]');

                if (toggleTextOn) {
                    toggleText($toggle, toggleTextOn, toggleTextOff);
                }

                $content.style.maxHeight = $content.scrollHeight + 'px';
                $collapsible.classList.toggle(collapsedClass);
            }
        });
    }

    window.addEventListener("resize", function(e) {
        responsiveCollapsibleHeight();
    });
    window.addEventListener("orientationchange", function(e) {
        responsiveCollapsibleHeight();
    });

    function responsiveCollapsibleHeight() {
        let windowWidth = window.innerWidth;

        for (i = 0; i < $collapsibleContents.length; i++) {
            $collapsibleContents[i].style.maxHeight = '';
            $collapsibleContents[i].style.maxHeight = $collapsibleContents[i].scrollHeight + 'px';
        }
    }
});
'use strict';

const activeClass    = 'is-active';
const collapsedClass = 'is-collapsed';
const disabledClass  = 'is-disabled';
const expandedClass  = 'is-expanded';
const filledClass    = 'is-filled';
const fixedClass     = 'is-fixed';
const focusClass     = 'is-focused';
const hoverClass     = 'is-hover';
const invisibleClass = 'is-invisible';
const selectedClass  = 'is-selected';
const scrolledClass  = 'is-scrolled';
const visibleClass   = 'is-visible';

const lockedScrollClass  = 'scroll-is-locked';
const mobileMenuVisibleClass = 'mobile-menu-is-visible';

const $body = $('.body');
const $main = $('.main');
const $footer = $('.footer');
const $header = $('[data-component="header"]');

const bpXS  = 320;
const bpSM  = 576;
const bpMD  = 768;
const bpLG  = 1024;
const bpXL = 1440;

const gridOffset = 24;

Dropzone.autoDiscover = false;

$(document).ready(function () {
    let previewNode = $('[data-element="dropzone-template"]');
    let previewTemplate = previewNode.html();
    previewNode.parent().html('');

    $('[data-dropzone-block]').dropzone({
        maxFiles: 10,
        maxFilesize: 10,
        url: "/ajax_file_upload_handler/",
        previewTemplate: previewTemplate,
        autoQueue: false, // Make sure the files aren't queued until manually added
        previewsContainer: '[data-element="dropzone-previews"]', // Define the container to display the previews
        clickable: '[data-element="dropzone-trigger"]', // Define the element that should be used as click trigger to select files.
        dictFileTooBig: 'File is over 10 Мb',
        success: function (file, response) {
            $(file.previewElement).find('[data-dz-uploadprogress]').parent().hide();
        },
        error: function (file, response) {
            let item = $(file.previewElement);
            let itemError = $(file.previewElement).find('[data-dz-errormessage]');
            let itemName = $(file.previewElement).find('[data-dz-name]');

            if (file && response) {
                item.addClass('has-error');
                itemName.hide();
                itemError.html(response)
            } else {
                item.removeClass('has-error');
                itemName.show();
                itemError.html('')
            }
        },
        uploadprogress: function(progress) {
            $(file.previewElement).find('[data-dz-uploadprogress]').css('width', progress + '%');
        },
    });
});
/* header on scroll */
document.addEventListener('DOMContentLoaded', function() {
    const $header = document.querySelector('[data-component="header"]');

    if (!$header) {
        return false;
    }

    function headerDeafultScroll() {
        let onScroll = function () {
            $header.classList.toggle(scrolledClass, window.scrollY > $header.offsetHeight);
        };

        onScroll();
        window.addEventListener('scroll', function() {
            onScroll();
        });
    }

    headerDeafultScroll();
});
let $phoneInputs = document.querySelectorAll('[data-mask="phone"]');

if ($phoneInputs.length !== 0) {
    for (const inputPhone of $phoneInputs) {
        Inputmask({"mask": "+7 (999) 999-99-99"}).mask(inputPhone);
    }
}
/* Mobile menu */
let $mobileMenu = $('[data-component="mobile-menu"]');
let $mobileMenuTrigger = $('[data-element="mobile-menu-trigger"]');
let $mobileMenuClose = $('[data-element="mobile-menu-close"]');

function hideMobileMenu() {
    $mobileMenuTrigger.removeClass(activeClass);
    $mobileMenu.removeClass(visibleClass);

    if ($body.hasClass(mobileMenuVisibleClass)) {
       $body.removeClass(mobileMenuVisibleClass);
       $body.removeClass(lockedScrollClass);
    }
}

function showMobileMenu(trigger, component) {
    if ($(window).width() < bpXL) {
        trigger.addClass(activeClass);
        component.addClass(visibleClass);
        $body.addClass(mobileMenuVisibleClass);
        $body.addClass(lockedScrollClass);
    }
}

function desktopCheck() {
    if ($(window).width() >= bpXL) {
        hideMobileMenu();
    }
}

$(document).ready(function(){

    $mobileMenuTrigger.on('click', function(e) {
        e.preventDefault();

        let $trigger = $(this);
        let triggerSrc = $trigger.attr('data-src');
        let $component = $('[data-component="mobile-menu"][data-src="'+ triggerSrc +'"]');

        if ($component.hasClass(visibleClass)) {
            hideMobileMenu();
        } else {
            $('[data-component="mobile-menu"]').removeClass(visibleClass);
            showMobileMenu($trigger, $component);
        }
    });

    $mobileMenuClose.on('click', function(e) {
        e.preventDefault();

        hideMobileMenu();
    });

    $(document).on('keyup', function(e) {
        if ($(window).width() < bpXL) {
            if (e.key === "Escape" || e.keyCode === 27) {
                hideMobileMenu();
            }
        }
    });

    $(document).on('click', function(e) {
        if (($(window).width() < bpXL)&&($(window).width() > 900)) {
            if ($mobileMenu.has(e.target).length === 0 && !$mobileMenu.is(e.target) && $mobileMenuTrigger.has(e.target).length === 0 && !$mobileMenuTrigger.is(e.target)) {
              if(!$(e.target).hasClass('subnav__two-link')) {
                hideMobileMenu();
              }
            }
        }
    });

    desktopCheck();

    $(window).on('orientationchange resize', function() {
        desktopCheck();
    });
});
/* Modal */
$(function() {
    $('[data-element="modal-trigger"]').magnificPopup({
        type: 'inline',
        fixedContentPos: true,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        callbacks: {
            beforeOpen: function() {
                let effectData = this.st.el.attr('data-effect');

                if (!effectData) {
                    effectData = 'mfp-zoom-in';
                }

                this.st.mainClass = effectData;
            },
            open: function() {
                $('.body').css('overflow', "hidden");
                let $mfp = $.magnificPopup.instance;
                let $trigger = $($mfp.currItem.el[0]);
                $trigger.attr('disabled', true);
            },
            close: function() {
                $('.body').css('overflow', "");
                let $mfp = $.magnificPopup.instance;
                let $trigger = $($mfp.currItem.el[0]);
                $trigger.removeAttr('disabled');
            }
        },
    });

    /* Gallery Modal */
    $('[data-gallery]').each(function() {
        $(this).magnificPopup({
            delegate: '[data-element="gallery-trigger"]', // the selector for gallery item
            type: 'image',
            closeOnContentClick: false,
            fixedContentPos: true,
            fixedBgPos: true,
            mainClass: 'mfp-zoom-in',
            image: {
              verticalFit: true,
              titleSrc: 'title',
            },
            gallery: {
              enabled: true
            },
            zoom: {
              enabled: true,
              duration: 300, // don't foget to change the duration also in CSS
              opener: function(element) {
                return element.find('img');
              }
            },
            callbacks: {
                buildControls: function() {
                    // re-appends controls inside the main container
                    this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                }
            }
        });
    });
    /*$('[data-element="gallery-trigger"]').magnificPopup({
        type: 'image',
        closeOnContentClick: false,
        fixedContentPos: true,
        fixedBgPos: true,
        mainClass: 'mfp-zoom-in',
        image: {
          verticalFit: true,
          titleSrc: 'title'
        },
        gallery: {
          enabled: true
        },
        zoom: {
          enabled: true,
          duration: 300, // don't foget to change the duration also in CSS
          opener: function(element) {
            return element.find('img');
          }
        },
        callbacks: {
            buildControls: function() {
                // re-appends controls inside the main container
                this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
            }
        }
    });*/

    $(document).on('click', '[data-element="modal-close"]', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
});

// Validation errors messages for Parsley
// Load this after Parsley
Parsley.addMessages('ru', {
    dateiso:  "Это значение должно быть корректной датой (ГГГГ-ММ-ДД).",
    minwords: "Это значение должно содержать не менее %s слов.",
    maxwords: "Это значение должно содержать не более %s слов.",
    words:    "Это значение должно содержать от %s до %s слов.",
    gt:       "Это значение должно быть больше.",
    gte:      "Это значение должно быть больше или равно.",
    lt:       "Это значение должно быть меньше.",
    lte:      "Это значение должно быть меньше или равно.",
    notequalto: "Это значение должно отличаться."
});

Parsley.addMessages('ru', {
    defaultMessage: "Некорректное значение.",
    type: {
        email:        "Введите адрес электронной почты.",
        url:          "Введите URL адрес.",
        number:       "Введите число.",
        integer:      "Введите целое число.",
        digits:       "Введите только цифры.",
        alphanum:     "Введите буквенно-цифровое значение."
    },
    notblank:       "Обязательное поле",
    required:       "Обязательное поле",
    pattern:        "Это значение некорректно.",
    min:            "Это значение должно быть не менее чем %s.",
    max:            "Это значение должно быть не более чем %s.",
    range:          "Это значение должно быть от %s до %s.",
    minlength:      "Это значение должно содержать не менее %s символов.",
    maxlength:      "Это значение должно содержать не более %s символов.",
    length:         "Это значение должно содержать от %s до %s символов.",
    mincheck:       "Выберите не менее %s значений.",
    maxcheck:       "Выберите не более %s значений.",
    check:          "Выберите от %s до %s значений.",
    equalto:        "Это значение должно совпадать."
});

Parsley.setLocale('ru');

/* Forms */
$(document).ready(function() {
    /* Select2 For parsley validation */
    $('[data-element="select"]').change(function() {
        $(this).trigger('input');
        $(this).parsley().validate();
    });

    /* Form Parsley Validation */
    let $parsleyForm = $('[data-parsley-validate]');
    let $parsleyFormSubmit = $parsleyForm.find('[type="submit"]');

    $parsleyForm.parsley({
        excluded: "[disabled], :hidden",
        errorClass: 'has-error',
        successClass: 'has-success',
        errorsWrapper: '<div class="form__errors-list"></div>',
        errorTemplate: '<div class="form__error"></div>',
        errorsContainer (field) {
            return field.$element.parent().closest('.form__group');
        },
        classHandler (field) {
            const $parent = field.$element.closest('.form__group');
            if ($parent.length) return $parent;

            return $parent;
        }
    });

    $('[data-parsley-validate]').on('submit', function(e) {
        e.preventDefault();

        const $form = $(this);
    });

});
/* Reviews video */
(function(){
    let $videoReviews = Array.from(document.querySelectorAll('[data-element="video-review"]'));

    let player;
    let done = false;

    $videoReviews.forEach((review) => {
        let video = review.querySelector('iframe');

        if (video) {
            review.addEventListener('mouseover', function(e) {
                onYouTubeIframeAPIReady(video);
             });

            review.addEventListener('mouseleave', function(e) {
            });
        }
    });

    function onYouTubeIframeAPIReady(video) {
        let videoID = video.id;
        player = new YT.Player(videoID, {
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
        player.playVideo();
    }

    function onPlayerReady(event) {
        event.target.playVideo();
    }

    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            setTimeout(stopVideo, 6000);
            done = true;
        }
    }
    function stopVideo() {
        player.stopVideo();
    }

})();

/* Select2 */
$(document).ready(function() {
    let selectCommonOptions = {
        dropdownCssClass: ':all:',
        containerCssClass: ':all:',
        minimumResultsForSearch: Infinity,
        width: '100%',
    };

    $('[data-element="select"]').select2(selectCommonOptions);

    $('[data-element="select-metro"]').select2(selectCommonOptions);

    $('[data-element="select-icon"]').select2({
        dropdownCssClass: ':all:',
        containerCssClass: ':all:',
        minimumResultsForSearch: Infinity,
        width: '100%',
        templateResult: formatSelect,
        templateSelection: formatSelect
    });

    function formatSelect (opt) {
        if (!opt.id) {
            return opt.text.toUpperCase();
        } 

        var optimage = $(opt.element).attr('data-icon'); 
        if(!optimage){
            return opt.text;
        } else {
            var $opt = $(
                '<i class="select2-icon icon-' + optimage + '"></i>'
            );
            return $opt;
        }
    }
});
/* Certificates slider */
(function(){
    let $certificatesAutoSliders = Array.from(document.querySelectorAll('[data-slider="certificates-auto"]'));

    $certificatesAutoSliders.forEach((slider) => {
        let $sliderContainer = slider.closest('[data-slider-container]')
        let nextButton = $sliderContainer.querySelector('.swiper-button-next');
        let prevButton = $sliderContainer.querySelector('.swiper-button-prev');
        let pagination = $sliderContainer.querySelector('.swiper-pagination');

        let $certificatesAutoSlider = new Swiper(slider, {
            slidesPerView: 'auto',
            spaceBetween: 7,
            watchOverflow: true,
            pagination: {
                el: pagination,
                clickable: true,
            },
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
        });
    });
})();
/* Certificates slider */
(function(){
    let $certificatesSliders = Array.from(document.querySelectorAll('[data-slider="certificates"]'));

    $certificatesSliders.forEach((slider) => {
        let $sliderContainer = slider.closest('[data-slider-container]')
        let nextButton = $sliderContainer.querySelector('.swiper-button-next');
        let prevButton = $sliderContainer.querySelector('.swiper-button-prev');
        let pagination = $sliderContainer.querySelector('.swiper-pagination');

        let $certificatesSlider = new Swiper(slider, {
            slidesPerView: 3,
            spaceBetween: 20,
            watchOverflow: true,
            pagination: {
                el: pagination,
                clickable: true,
            },
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
            breakpoints: {
                [bpXS]: {
                    slidesPerView: 1,
                },
                [bpSM]: {
                    slidesPerView: 2,
                },
                [bpLG]: {
                    slidesPerView: 3,
                },
            },
        });
    });
})();
/* Cover slider */
(function(){
    let $coverSliders = Array.from(document.querySelectorAll('[data-slider="cover"]'));

    $coverSliders.forEach((slider) => {
        let $coverSlider = new Swiper(slider, {
            slidesPerView: 1,
            spaceBetween: gridOffset,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    });
})();
/* Examples slider */
(function(){
    let $slider;
    const destroyBreakpoint = window.matchMedia( '(min-width:'+bpLG+'px)' );

    const breakpointChecker = function() {
        if ( destroyBreakpoint.matches === true ) {
            if ( $slider !== undefined ) $slider.destroy( true, true );
            return;
        } else if ( destroyBreakpoint.matches === false ) {
            return enableSwiper();
        }
    };

    const enableSwiper = function() {
        $slider = new Swiper ('[data-slider="examples"]', {
            slidesPerView: 'auto',
            spaceBetween: gridOffset,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
        });
    };

    destroyBreakpoint.addListener(breakpointChecker);

    breakpointChecker();
})();
/* Gallery slider */
(function(){
    let $slider;
    const destroyBreakpoint = window.matchMedia( '(min-width:'+bpLG+'px)' );

    const breakpointChecker = function() {
        if ( destroyBreakpoint.matches === true ) {
            if ( $slider !== undefined ) $slider.destroy( true, true );
            return;
        } else if ( destroyBreakpoint.matches === false ) {
            return enableSwiper();
        }
    };

    const enableSwiper = function() {
        $slider = new Swiper ('[data-slider="gallery"]', {
            slidesPerView: 1,
            spaceBetween: gridOffset,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
        });
    };

    destroyBreakpoint.addListener(breakpointChecker);

    breakpointChecker();
})();
/* Offers slider */
(function(){
    let $offersSliders = Array.from(document.querySelectorAll('[data-slider="offers"]'));

    $offersSliders.forEach((slider) => {
        let $offersSlider = new Swiper(slider, {
            slidesPerView: 1,
            spaceBetween: 20,
            watchOverflow: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
        });
    });
})();
/* Ratingbar slider */
(function(){
    let $ratingbarSlider = new Swiper('[data-slider="ratingbar"]', {
        slidesPerView: 'auto',
        spaceBetween: 47,
        watchOverflow: true,
        breakpoints: {
            [bpXL]: {
                slidesPerView: 6,
            },
        },
    });
})();
/* Reviews slider */
(function(){
    let $reviewsSliders = Array.from(document.querySelectorAll('[data-slider="reviews"]'));
    const destroyBreakpoint = window.matchMedia( '(min-width:'+bpLG+'px)' );

    if ($reviewsSliders.length === 0) {
        return false;
    }

    $reviewsSliders.forEach((slider) => {
        let $slider;

        const breakpointChecker = function() {
            if ( destroyBreakpoint.matches === true ) {
                if ( $slider !== undefined ) $slider.destroy( true, true );
                return;
            } else if ( destroyBreakpoint.matches === false ) {
                return enableSwiper();
            }
        };

        const enableSwiper = function() {
            $slider = new Swiper (slider, {
                slidesPerView: 1,
                spaceBetween: gridOffset,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                breakpoints: {
                    [bpXS]: {
                        slidesPerView: 1,
                    },
                    [bpMD]: {
                        slidesPerView: 2,
                    },
                },
            });
        };

        destroyBreakpoint.addListener(breakpointChecker);

        breakpointChecker();
    });
})();
/* Staff slider */
(function(){
    let $staffSliders = Array.from(document.querySelectorAll('[data-slider="staff"]'));

    $staffSliders.forEach((slider) => {
        let $sliderContainer = slider.closest('[data-slider-container]')
        let nextButton = $sliderContainer.querySelector('.swiper-button-next');
        let prevButton = $sliderContainer.querySelector('.swiper-button-prev');
        let pagination = $sliderContainer.querySelector('.swiper-pagination');

        let $staffSlider = new Swiper(slider, {
            slidesPerView: 3,
            spaceBetween: 40,
            pagination: {
                el: pagination,
                clickable: true,
            },
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
            breakpoints: {
                [bpXS]: {
                    slidesPerView: 1,
                    spaceBetween: gridOffset,
                },
                [bpMD]: {
                    slidesPerView: 2,
                    spaceBetween: gridOffset,
                },
                [bpLG]: {
                    slidesPerView: 3,
                    spaceBetween: gridOffset,
                },
                [bpXL]: {
                    slidesPerView: 3,
                    spaceBetween: 40,
                },
            },
        });
    });
})();
/* Tabs slider */
(function(){
    let $tabsSliders = Array.from(document.querySelectorAll('[data-slider="tabs"]'));
    const destroyBreakpoint = window.matchMedia( '(min-width:'+bpLG+'px)' );

    if ($tabsSliders.length === 0) {
        return false;
    }

    $tabsSliders.forEach((slider) => {
        let activeSlideIndex = 0;

        if (slider.querySelector('.is-active')) {
            let $activeSlide = slider.querySelector('.is-active').parentElement;
            activeSlideIndex = [].indexOf.call($activeSlide.parentElement.children, $activeSlide);
        }

        let $slider;

        const breakpointChecker = function() {
            if ( destroyBreakpoint.matches === true ) {
                if ( $slider !== undefined ) $slider.destroy( true, true );
                return;
            } else if ( destroyBreakpoint.matches === false ) {
                return enableSwiper();
            }
        };

        const enableSwiper = function() {
            $slider = new Swiper (slider, {
                slidesPerView: "auto",
                slideToClickedSlide: true,
                initialSlide: activeSlideIndex,
            });
        };

        destroyBreakpoint.addListener(breakpointChecker);

        breakpointChecker();
    });
})();
let scrollOptions = {
    speed: 1000,
    easing: 'easeInOutCubic',
    header: '[data-component="header"]',
    offset: 100,
};

let scroll = new SmoothScroll('[data-scroll]', scrollOptions);
(function(){
  const HEADER_OFFSET_XL = 190; // px
  const HEADER_OFFSET_LG = 80; // px

  const breakpointMD = window.matchMedia(`(min-width: ${bpMD}px`);
  const breakpointXL = window.matchMedia(`(min-width: ${bpXL}px`);
  const containers =document.querySelectorAll('[ data-sctiky-container]');

  if (!containers.length) {
    return;
  }

  let headerOffset;

  const setBlockWidth = (parent, el) => {
    el.style.width = `${parent.clientWidth - gridOffset}px`;
  }

  containers.forEach((container) => {
    const block = container.querySelector('[data-sctiky-block]');

    if (!block) {
      return;
    }

    let lastPageYOffset = window.pageYOffset;
    let isScrollForward;

    setBlockWidth(container, block);

    const onScroll = () => {

      const containerTop = container.getBoundingClientRect().top;
      const containerBottom = containerTop + container.clientHeight;
      const blockTop = block.getBoundingClientRect().top;
      const blockBottom = blockTop + block.clientHeight;

      if (breakpointMD.matches) {
        if (block.clientHeight > container.clientHeight - gridOffset) {
          return;
        };

        isScrollForward = window.pageYOffset >= lastPageYOffset;
        lastPageYOffset = window.pageYOffset;

        if (isScrollForward) {
          if (containerTop <= headerOffset) {
            if (containerBottom - gridOffset - 1> blockBottom) {
              if (block.style.position !== 'fixed') {
                block.style.position = 'fixed';
                block.style.top = `${headerOffset}px`;
                block.style.bottom = null;
              }
            } else {
              if (block.style.position !== 'absolute') {
                block.style.position = 'absolute';
                block.style.top = null;
                block.style.bottom = `${gridOffset}px`;
              }
            }
          }
        } else {
          if (blockTop >= headerOffset) {
            if (blockTop > containerTop) {
              if (block.style.position !== 'fixed') {
                block.style.position = 'fixed';
                block.style.top = `${headerOffset}px`;
                block.style.bottom = null;
              }
            } else {
              block.style.position = null;
              block.style.top = null;
              block.style.bottom = null;
            }
          }
        }
      } else {
        block.style.position = null;
        block.style.top = null;
        block.style.bottom = null;
      }
    };

    const breakpointXLChecker = () => {

      if (breakpointXL.matches) {
        headerOffset = HEADER_OFFSET_XL;
      } else {
        headerOffset = HEADER_OFFSET_LG;
      }
    };


    breakpointXLChecker();
    onScroll();

    breakpointXL.addListener(breakpointXLChecker);

    window.addEventListener('resize', () => {
      setBlockWidth(container, block);
      breakpointXLChecker();
      onScroll();
    });

    window.addEventListener('scroll', onScroll);
  });
})();

(function(){
  const navLink = document.querySelectorAll('.nav__item a');
  const navLinkTwo = document.querySelectorAll('.subnav__two-link');
  const navBlockArr = document.querySelectorAll('.subnav__item-block');
  const navMobileBlockArr = document.querySelectorAll('.subnav__item-mobile');
  const subnavArr = document.querySelectorAll('.subnav');
  const hamburger = document.querySelector('.hamburger');
  const header = document.querySelector('.header');
  const headerCloseSubnav = document.querySelector('.header__close-subnav');

  navLink.forEach(item => {
    if (item.dataset.subnav) {
      item.addEventListener('click' , function (evt) {
        evt.preventDefault();
        navLink.forEach(item2 => {
          if (item2 === item) {
            item2.classList.toggle('active');
          } else {
            item2.classList.remove('active');
          }
        })
        subnavArr.forEach(subnav => {
          if (subnav.dataset.subnav === item.dataset.subnav) {
            subnav.classList.toggle('active');
          } else {
            subnav.classList.remove('active');
          }
        })
        // Включаем хедер в режим субнав
        header.classList.add('open-subnav');
      })
    }
  })

  navLinkTwo.forEach(item => {
    item.addEventListener('click' , function (evt) {
      evt.preventDefault();
      item.classList.toggle('active');
      navBlockArr.forEach(navBlock => {
        navBlock.classList.remove('active');
      })
      navBlockArr.forEach(navBlock => {
        if (item.dataset.subnavtwo === navBlock.dataset.subnavtwo) {
          navBlock.classList.add('active');
        }
      })
    })
  })

  $(navLinkTwo).on('click' , function() {
    $(this).next().addClass('active');
  })

  hamburger.addEventListener('click' , function() {
    navLink.forEach(item => {
      item.classList.remove('active');
    })
    subnavArr.forEach(item => {
      item.classList.remove('active');
    })
    // Выключаем хедер в режим субнав
    $(header).removeClass('open-subnav');
    $(navMobileBlockArr).removeClass('active');
  })

  $(headerCloseSubnav).on('click' , function() {
    if($(navMobileBlockArr).hasClass('active')) {
      $(navMobileBlockArr).removeClass('active');
    } else {
      $(subnavArr).removeClass('active');
      $(header).removeClass('open-subnav');
    }
  })
})();
/* Tabs */
document.addEventListener('DOMContentLoaded', function() {
    let $tabToggles = document.querySelectorAll('[data-element="tabs-toggle"]');
    let i = 0;

    if ($tabToggles.length > 0 ) {
        for (i = 0; i < $tabToggles.length; i++) {
            $tabToggles[i].addEventListener("click", function(e) {
                e.preventDefault();

                let $toggle = this;
                let $tabs = $toggle.closest('[data-component="tabs"]');
                let tabID;

                if ($toggle.hasAttribute('href')) {
                    tabID = $toggle.getAttribute('href').slice(1);
                }

                if ($toggle.hasAttribute('data-src')) {
                    tabID = $toggle.dataset.src;
                }

                let $tabActive = '[data-element="tabs-tab"][data-id="' + tabID +'"]';

                if (!($toggle.classList.contains(activeClass))) {
                    $tabs.querySelector('.' + activeClass +'[data-element="tabs-tab"]').classList.remove(activeClass);
                    $tabs.querySelector('.' + activeClass +'[data-element="tabs-toggle"]').classList.remove(activeClass);
                    $tabs.querySelector($tabActive).classList.add(activeClass);
                    $toggle.classList.add(activeClass);
                }
            });
        }
    }
});
