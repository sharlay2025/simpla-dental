/* Staff slider */
(function(){
    let $staffSliders = Array.from(document.querySelectorAll('[data-slider="staff"]'));

    $staffSliders.forEach((slider) => {
        let $sliderContainer = slider.closest('[data-slider-container]')
        let nextButton = $sliderContainer.querySelector('.swiper-button-next');
        let prevButton = $sliderContainer.querySelector('.swiper-button-prev');
        let pagination = $sliderContainer.querySelector('.swiper-pagination');

        let $staffSlider = new Swiper(slider, {
            slidesPerView: 3,
            spaceBetween: 40,
            pagination: {
                el: pagination,
                clickable: true,
            },
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
            breakpoints: {
                [bpXS]: {
                    slidesPerView: 1,
                    spaceBetween: gridOffset,
                },
                [bpMD]: {
                    slidesPerView: 2,
                    spaceBetween: gridOffset,
                },
                [bpLG]: {
                    slidesPerView: 3,
                    spaceBetween: gridOffset,
                },
                [bpXL]: {
                    slidesPerView: 3,
                    spaceBetween: 40,
                },
            },
        });
    });
})();