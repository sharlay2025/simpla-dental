/* Mobile menu */
let $mobileMenu = $('[data-component="mobile-menu"]');
let $mobileMenuTrigger = $('[data-element="mobile-menu-trigger"]');
let $mobileMenuClose = $('[data-element="mobile-menu-close"]');

function hideMobileMenu() {
    $mobileMenuTrigger.removeClass(activeClass);
    $mobileMenu.removeClass(visibleClass);

    if ($body.hasClass(mobileMenuVisibleClass)) {
       $body.removeClass(mobileMenuVisibleClass);
       $body.removeClass(lockedScrollClass);
    }
}

function showMobileMenu(trigger, component) {
    if ($(window).width() < bpXL) {
        trigger.addClass(activeClass);
        component.addClass(visibleClass);
        $body.addClass(mobileMenuVisibleClass);
        $body.addClass(lockedScrollClass);
    }
}

function desktopCheck() {
    if ($(window).width() >= bpXL) {
        hideMobileMenu();
    }
}

$(document).ready(function(){

    $mobileMenuTrigger.on('click', function(e) {
        e.preventDefault();

        let $trigger = $(this);
        let triggerSrc = $trigger.attr('data-src');
        let $component = $('[data-component="mobile-menu"][data-src="'+ triggerSrc +'"]');

        if ($component.hasClass(visibleClass)) {
            hideMobileMenu();
        } else {
            $('[data-component="mobile-menu"]').removeClass(visibleClass);
            showMobileMenu($trigger, $component);
        }
    });

    $mobileMenuClose.on('click', function(e) {
        e.preventDefault();

        hideMobileMenu();
    });

    $(document).on('keyup', function(e) {
        if ($(window).width() < bpXL) {
            if (e.key === "Escape" || e.keyCode === 27) {
                hideMobileMenu();
            }
        }
    });

    $(document).on('click', function(e) {
        if (($(window).width() < bpXL)&&($(window).width() > 900)) {
            if ($mobileMenu.has(e.target).length === 0 && !$mobileMenu.is(e.target) && $mobileMenuTrigger.has(e.target).length === 0 && !$mobileMenuTrigger.is(e.target)) {
              if(!$(e.target).hasClass('subnav__two-link')) {
                hideMobileMenu();
              }
            }
        }
    });

    desktopCheck();

    $(window).on('orientationchange resize', function() {
        desktopCheck();
    });
});