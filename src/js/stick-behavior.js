(function(){
  const HEADER_OFFSET_XL = 190; // px
  const HEADER_OFFSET_LG = 80; // px

  const breakpointMD = window.matchMedia(`(min-width: ${bpMD}px`);
  const breakpointXL = window.matchMedia(`(min-width: ${bpXL}px`);
  const containers =document.querySelectorAll('[ data-sctiky-container]');

  if (!containers.length) {
    return;
  }

  let headerOffset;

  const setBlockWidth = (parent, el) => {
    el.style.width = `${parent.clientWidth - gridOffset}px`;
  }

  containers.forEach((container) => {
    const block = container.querySelector('[data-sctiky-block]');

    if (!block) {
      return;
    }

    let lastPageYOffset = window.pageYOffset;
    let isScrollForward;

    setBlockWidth(container, block);

    const onScroll = () => {

      const containerTop = container.getBoundingClientRect().top;
      const containerBottom = containerTop + container.clientHeight;
      const blockTop = block.getBoundingClientRect().top;
      const blockBottom = blockTop + block.clientHeight;

      if (breakpointMD.matches) {
        if (block.clientHeight > container.clientHeight - gridOffset) {
          return;
        };

        isScrollForward = window.pageYOffset >= lastPageYOffset;
        lastPageYOffset = window.pageYOffset;

        if (isScrollForward) {
          if (containerTop <= headerOffset) {
            if (containerBottom - gridOffset - 1> blockBottom) {
              if (block.style.position !== 'fixed') {
                block.style.position = 'fixed';
                block.style.top = `${headerOffset}px`;
                block.style.bottom = null;
              }
            } else {
              if (block.style.position !== 'absolute') {
                block.style.position = 'absolute';
                block.style.top = null;
                block.style.bottom = `${gridOffset}px`;
              }
            }
          }
        } else {
          if (blockTop >= headerOffset) {
            if (blockTop > containerTop) {
              if (block.style.position !== 'fixed') {
                block.style.position = 'fixed';
                block.style.top = `${headerOffset}px`;
                block.style.bottom = null;
              }
            } else {
              block.style.position = null;
              block.style.top = null;
              block.style.bottom = null;
            }
          }
        }
      } else {
        block.style.position = null;
        block.style.top = null;
        block.style.bottom = null;
      }
    };

    const breakpointXLChecker = () => {

      if (breakpointXL.matches) {
        headerOffset = HEADER_OFFSET_XL;
      } else {
        headerOffset = HEADER_OFFSET_LG;
      }
    };


    breakpointXLChecker();
    onScroll();

    breakpointXL.addListener(breakpointXLChecker);

    window.addEventListener('resize', () => {
      setBlockWidth(container, block);
      breakpointXLChecker();
      onScroll();
    });

    window.addEventListener('scroll', onScroll);
  });
})();
