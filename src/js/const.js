'use strict';

const activeClass    = 'is-active';
const collapsedClass = 'is-collapsed';
const disabledClass  = 'is-disabled';
const expandedClass  = 'is-expanded';
const filledClass    = 'is-filled';
const fixedClass     = 'is-fixed';
const focusClass     = 'is-focused';
const hoverClass     = 'is-hover';
const invisibleClass = 'is-invisible';
const selectedClass  = 'is-selected';
const scrolledClass  = 'is-scrolled';
const visibleClass   = 'is-visible';

const lockedScrollClass  = 'scroll-is-locked';
const mobileMenuVisibleClass = 'mobile-menu-is-visible';

const $body = $('.body');
const $main = $('.main');
const $footer = $('.footer');
const $header = $('[data-component="header"]');

const bpXS  = 320;
const bpSM  = 576;
const bpMD  = 768;
const bpLG  = 1024;
const bpXL = 1440;

const gridOffset = 24;
