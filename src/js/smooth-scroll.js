let scrollOptions = {
    speed: 1000,
    easing: 'easeInOutCubic',
    header: '[data-component="header"]',
    offset: 100,
};

let scroll = new SmoothScroll('[data-scroll]', scrollOptions);