/* Certificates slider */
(function(){
    let $certificatesSliders = Array.from(document.querySelectorAll('[data-slider="certificates"]'));

    $certificatesSliders.forEach((slider) => {
        let $sliderContainer = slider.closest('[data-slider-container]')
        let nextButton = $sliderContainer.querySelector('.swiper-button-next');
        let prevButton = $sliderContainer.querySelector('.swiper-button-prev');
        let pagination = $sliderContainer.querySelector('.swiper-pagination');

        let $certificatesSlider = new Swiper(slider, {
            slidesPerView: 3,
            spaceBetween: 20,
            watchOverflow: true,
            pagination: {
                el: pagination,
                clickable: true,
            },
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
            breakpoints: {
                [bpXS]: {
                    slidesPerView: 1,
                },
                [bpSM]: {
                    slidesPerView: 2,
                },
                [bpLG]: {
                    slidesPerView: 3,
                },
            },
        });
    });
})();