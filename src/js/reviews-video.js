/* Reviews video */
(function(){
    let $videoReviews = Array.from(document.querySelectorAll('[data-element="video-review"]'));

    let player;
    let done = false;

    $videoReviews.forEach((review) => {
        let video = review.querySelector('iframe');

        if (video) {
            review.addEventListener('mouseover', function(e) {
                onYouTubeIframeAPIReady(video);
             });

            review.addEventListener('mouseleave', function(e) {
            });
        }
    });

    function onYouTubeIframeAPIReady(video) {
        let videoID = video.id;
        player = new YT.Player(videoID, {
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
        player.playVideo();
    }

    function onPlayerReady(event) {
        event.target.playVideo();
    }

    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
            setTimeout(stopVideo, 6000);
            done = true;
        }
    }
    function stopVideo() {
        player.stopVideo();
    }

})();
