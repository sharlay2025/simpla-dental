/* Tabs slider */
(function(){
    let $tabsSliders = Array.from(document.querySelectorAll('[data-slider="tabs"]'));
    const destroyBreakpoint = window.matchMedia( '(min-width:'+bpLG+'px)' );

    if ($tabsSliders.length === 0) {
        return false;
    }

    $tabsSliders.forEach((slider) => {
        let activeSlideIndex = 0;

        if (slider.querySelector('.is-active')) {
            let $activeSlide = slider.querySelector('.is-active').parentElement;
            activeSlideIndex = [].indexOf.call($activeSlide.parentElement.children, $activeSlide);
        }

        let $slider;

        const breakpointChecker = function() {
            if ( destroyBreakpoint.matches === true ) {
                if ( $slider !== undefined ) $slider.destroy( true, true );
                return;
            } else if ( destroyBreakpoint.matches === false ) {
                return enableSwiper();
            }
        };

        const enableSwiper = function() {
            $slider = new Swiper (slider, {
                slidesPerView: "auto",
                slideToClickedSlide: true,
                initialSlide: activeSlideIndex,
            });
        };

        destroyBreakpoint.addListener(breakpointChecker);

        breakpointChecker();
    });
})();