/* Ratingbar slider */
(function(){
    let $ratingbarSlider = new Swiper('[data-slider="ratingbar"]', {
        slidesPerView: 'auto',
        spaceBetween: 47,
        watchOverflow: true,
        breakpoints: {
            [bpXL]: {
                slidesPerView: 6,
            },
        },
    });
})();