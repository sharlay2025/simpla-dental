/* Examples slider */
(function(){
    let $slider;
    const destroyBreakpoint = window.matchMedia( '(min-width:'+bpLG+'px)' );

    const breakpointChecker = function() {
        if ( destroyBreakpoint.matches === true ) {
            if ( $slider !== undefined ) $slider.destroy( true, true );
            return;
        } else if ( destroyBreakpoint.matches === false ) {
            return enableSwiper();
        }
    };

    const enableSwiper = function() {
        $slider = new Swiper ('[data-slider="examples"]', {
            slidesPerView: 'auto',
            spaceBetween: gridOffset,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
        });
    };

    destroyBreakpoint.addListener(breakpointChecker);

    breakpointChecker();
})();