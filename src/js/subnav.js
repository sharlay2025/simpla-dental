(function(){
  const navLink = document.querySelectorAll('.nav__item a');
  const navLinkTwo = document.querySelectorAll('.subnav__two-link');
  const navBlockArr = document.querySelectorAll('.subnav__item-block');
  const navMobileBlockArr = document.querySelectorAll('.subnav__item-mobile');
  const subnavArr = document.querySelectorAll('.subnav');
  const hamburger = document.querySelector('.hamburger');
  const header = document.querySelector('.header');
  const headerCloseSubnav = document.querySelector('.header__close-subnav');

  navLink.forEach(item => {
    if (item.dataset.subnav) {
      item.addEventListener('click' , function (evt) {
        evt.preventDefault();
        navLink.forEach(item2 => {
          if (item2 === item) {
            item2.classList.toggle('active');
          } else {
            item2.classList.remove('active');
          }
        })
        subnavArr.forEach(subnav => {
          if (subnav.dataset.subnav === item.dataset.subnav) {
            subnav.classList.toggle('active');
          } else {
            subnav.classList.remove('active');
          }
        })
        // Включаем хедер в режим субнав
        header.classList.add('open-subnav');
      })
    }
  })

  navLinkTwo.forEach(item => {
    item.addEventListener('click' , function (evt) {
      evt.preventDefault();
      item.classList.toggle('active');
      navBlockArr.forEach(navBlock => {
        navBlock.classList.remove('active');
      })
      navBlockArr.forEach(navBlock => {
        if (item.dataset.subnavtwo === navBlock.dataset.subnavtwo) {
          navBlock.classList.add('active');
        }
      })
    })
  })

  $(navLinkTwo).on('click' , function() {
    $(this).next().addClass('active');
  })

  hamburger.addEventListener('click' , function() {
    navLink.forEach(item => {
      item.classList.remove('active');
    })
    subnavArr.forEach(item => {
      item.classList.remove('active');
    })
    // Выключаем хедер в режим субнав
    $(header).removeClass('open-subnav');
    $(navMobileBlockArr).removeClass('active');
  })

  $(headerCloseSubnav).on('click' , function() {
    if($(navMobileBlockArr).hasClass('active')) {
      $(navMobileBlockArr).removeClass('active');
    } else {
      $(subnavArr).removeClass('active');
      $(header).removeClass('open-subnav');
    }
  })
})();