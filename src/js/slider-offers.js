/* Offers slider */
(function(){
    let $offersSliders = Array.from(document.querySelectorAll('[data-slider="offers"]'));

    $offersSliders.forEach((slider) => {
        let $offersSlider = new Swiper(slider, {
            slidesPerView: 1,
            spaceBetween: 20,
            watchOverflow: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
        });
    });
})();