/* Beer slider */
document.addEventListener('DOMContentLoaded', function() {
    let $baSliders = document.querySelectorAll('[data-element="ba-slider"]:not([data-element-v2="ba-slider"])');
    const $baSlidersV2 = document.querySelectorAll('[data-element-v2="ba-slider"]');
    let i = 0;

    if ($baSliders.length + $baSlidersV2.length === 0) {
        return false;
    }

    for (i = 0; i < $baSliders.length; i++) {
        let $baSlider = new BeerSlider($baSliders[i], { start: 25 });
    }

    $baSlidersV2.forEach((slider) => {
      const baSlider = new BeerSlider(slider, { start: 50 });
    });
});
