/* Collapsible */
document.addEventListener('DOMContentLoaded', function() {
    let $collapsibleToggles = document.querySelectorAll('[data-element="collapsible-toggle"]');
    let $collapsibleContents = document.querySelectorAll('[data-element="collapsible-content"]');
    let i = 0;

    if ($collapsibleToggles.length === 0) {
        return false;
    }

    responsiveCollapsibleHeight();

    for (i = 0; i < $collapsibleToggles.length; i++) {
        $collapsibleToggles[i].addEventListener("click", function(e) {
            let $toggle = this;
            let toggleTextOn = $toggle.dataset.textOn;
            let toggleTextOff = $toggle.dataset.textOff;
            let toggleDisabledOn = $toggle.dataset.disabledOn;
            let windowWidth = window.innerWidth;

            if ((toggleDisabledOn === 'tablet' && windowWidth >= bpMD) || (toggleDisabledOn ==- 'desktop' && windowWidth >= bpXL)) {
                if ($toggle.hasAttribute('href')) {
                    window.location = this.href;
                } else {
                    e.preventDefault();
                }
            } else {
                e.preventDefault();

                let $collapsible   = $toggle.closest('[data-component="collapsible"]');
                let $content = $collapsible.querySelector('[data-element="collapsible-content"]');

                if (toggleTextOn) {
                    toggleText($toggle, toggleTextOn, toggleTextOff);
                }

                $content.style.maxHeight = $content.scrollHeight + 'px';
                $collapsible.classList.toggle(collapsedClass);
            }
        });
    }

    window.addEventListener("resize", function(e) {
        responsiveCollapsibleHeight();
    });
    window.addEventListener("orientationchange", function(e) {
        responsiveCollapsibleHeight();
    });

    function responsiveCollapsibleHeight() {
        let windowWidth = window.innerWidth;

        for (i = 0; i < $collapsibleContents.length; i++) {
            $collapsibleContents[i].style.maxHeight = '';
            $collapsibleContents[i].style.maxHeight = $collapsibleContents[i].scrollHeight + 'px';
        }
    }
});