/* Cover slider */
(function(){
    let $coverSliders = Array.from(document.querySelectorAll('[data-slider="cover"]'));

    $coverSliders.forEach((slider) => {
        let $coverSlider = new Swiper(slider, {
            slidesPerView: 1,
            spaceBetween: gridOffset,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    });
})();