/* Reviews slider */
(function(){
    let $reviewsSliders = Array.from(document.querySelectorAll('[data-slider="reviews"]'));
    const destroyBreakpoint = window.matchMedia( '(min-width:'+bpLG+'px)' );

    if ($reviewsSliders.length === 0) {
        return false;
    }

    $reviewsSliders.forEach((slider) => {
        let $slider;

        const breakpointChecker = function() {
            if ( destroyBreakpoint.matches === true ) {
                if ( $slider !== undefined ) $slider.destroy( true, true );
                return;
            } else if ( destroyBreakpoint.matches === false ) {
                return enableSwiper();
            }
        };

        const enableSwiper = function() {
            $slider = new Swiper (slider, {
                slidesPerView: 1,
                spaceBetween: gridOffset,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                breakpoints: {
                    [bpXS]: {
                        slidesPerView: 1,
                    },
                    [bpMD]: {
                        slidesPerView: 2,
                    },
                },
            });
        };

        destroyBreakpoint.addListener(breakpointChecker);

        breakpointChecker();
    });
})();