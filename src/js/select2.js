/* Select2 */
$(document).ready(function() {
    let selectCommonOptions = {
        dropdownCssClass: ':all:',
        containerCssClass: ':all:',
        minimumResultsForSearch: Infinity,
        width: '100%',
    };

    $('[data-element="select"]').select2(selectCommonOptions);

    $('[data-element="select-metro"]').select2(selectCommonOptions);

    $('[data-element="select-icon"]').select2({
        dropdownCssClass: ':all:',
        containerCssClass: ':all:',
        minimumResultsForSearch: Infinity,
        width: '100%',
        templateResult: formatSelect,
        templateSelection: formatSelect
    });

    function formatSelect (opt) {
        if (!opt.id) {
            return opt.text.toUpperCase();
        } 

        var optimage = $(opt.element).attr('data-icon'); 
        if(!optimage){
            return opt.text;
        } else {
            var $opt = $(
                '<i class="select2-icon icon-' + optimage + '"></i>'
            );
            return $opt;
        }
    }
});