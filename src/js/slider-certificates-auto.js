/* Certificates slider */
(function(){
    let $certificatesAutoSliders = Array.from(document.querySelectorAll('[data-slider="certificates-auto"]'));

    $certificatesAutoSliders.forEach((slider) => {
        let $sliderContainer = slider.closest('[data-slider-container]')
        let nextButton = $sliderContainer.querySelector('.swiper-button-next');
        let prevButton = $sliderContainer.querySelector('.swiper-button-prev');
        let pagination = $sliderContainer.querySelector('.swiper-pagination');

        let $certificatesAutoSlider = new Swiper(slider, {
            slidesPerView: 'auto',
            spaceBetween: 7,
            watchOverflow: true,
            pagination: {
                el: pagination,
                clickable: true,
            },
            navigation: {
                nextEl: nextButton,
                prevEl: prevButton,
            },
        });
    });
})();