/* Tabs */
document.addEventListener('DOMContentLoaded', function() {
    let $tabToggles = document.querySelectorAll('[data-element="tabs-toggle"]');
    let i = 0;

    if ($tabToggles.length > 0 ) {
        for (i = 0; i < $tabToggles.length; i++) {
            $tabToggles[i].addEventListener("click", function(e) {
                e.preventDefault();

                let $toggle = this;
                let $tabs = $toggle.closest('[data-component="tabs"]');
                let tabID;

                if ($toggle.hasAttribute('href')) {
                    tabID = $toggle.getAttribute('href').slice(1);
                }

                if ($toggle.hasAttribute('data-src')) {
                    tabID = $toggle.dataset.src;
                }

                let $tabActive = '[data-element="tabs-tab"][data-id="' + tabID +'"]';

                if (!($toggle.classList.contains(activeClass))) {
                    $tabs.querySelector('.' + activeClass +'[data-element="tabs-tab"]').classList.remove(activeClass);
                    $tabs.querySelector('.' + activeClass +'[data-element="tabs-toggle"]').classList.remove(activeClass);
                    $tabs.querySelector($tabActive).classList.add(activeClass);
                    $toggle.classList.add(activeClass);
                }
            });
        }
    }
});
